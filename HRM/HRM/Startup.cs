﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HRM.Model.Model;
using HRM.Models.Models;
using HRM.Reponsitory.Extension;
using HRM.Services.Extension;
using HRM.UnitOfWork.Shared;
using HRM.Models.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace HRM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
//            services.AddDbContext<DBContext>(opt =>
//                opt.UseSqlServer(Configuration.GetConnectionString("ConfluenceDBContext"),
//                    b => b.MigrationsAssembly("StartUp.Confluence.Model")));
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
//            services.AddDbContext<DBContext>(item => item.UseSqlServer(Configuration.GetConnectionString("myconn")));
            
            services.AddDbContext<DBContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString("DBContext"),
                    b => b.MigrationsAssembly("HRM")));
            services.AddTransient<IEntity, BaseEntity>();
            services.AddUnitOfWork<DBContext>();
            services.AddAutoMapper(typeof(Startup));
            services.AddRepositoriesInstance();
            services.AddServiceExtension();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "My API",
                    Description = "My First ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Talking Dotnet", Email = "contact@talkingdotnet.com", Url = "www.talkingdotnet.com" }
                });
            });
            services.AddCors();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger(c =>
            {

            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                
            });

            loggerFactory.AddFile("Log/mylog-{Date}.txt");
        }
    }
}