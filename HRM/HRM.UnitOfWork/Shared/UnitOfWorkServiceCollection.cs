﻿using HRM.UnitOfWork.Reponsitory;
using HRM.Models.Models;
using Microsoft.Extensions.DependencyInjection;

namespace HRM.UnitOfWork.Shared
{
    public static class UnitOfWorkServiceCollection
    {
        public static IServiceCollection AddUnitOfWork<TContext>(this IServiceCollection services)
            where TContext : DBContext
        {
            services.AddTransient<IUnitOfWork, UnitOfWork<TContext>>();
            services.AddTransient<IUnitOfWork<TContext>, UnitOfWork<TContext>>();

            return services;
        }
    }
}