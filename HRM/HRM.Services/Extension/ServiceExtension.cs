﻿using HRM.Reponsitory.Reponsitory;
using HRM.Reponsitory.Shared;
using HRM.Services.Implement;
using HRM.Services.Shared;
using Microsoft.Extensions.DependencyInjection;

namespace HRM.Services.Extension
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServiceExtension(this IServiceCollection service)
        {
            service.AddTransient<IUserService, UserService>();
            
            return service;
        }
    }
}