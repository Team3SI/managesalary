﻿using HRM.Model.Model;
using HRM.Services.Collection;
using HRM.Models.Models;

namespace HRM.Services.Shared
{
    public interface IUserService : IBaseService<User>
    {
        
    }
}