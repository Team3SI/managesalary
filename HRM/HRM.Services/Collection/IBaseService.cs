﻿using System;
using System.Threading.Tasks;
using HRM.UnitOfWork.Collection;

namespace HRM.Services.Collection
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        Task<IPagedList<TEntity>> GetAllAsync();        
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
        TEntity Get(Guid id);
        TEntity Delete(Guid id);
    }
}